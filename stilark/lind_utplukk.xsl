<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:ubfu="http://uib.no/ub/"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.tei-c.org/ns/1.0"
    version="3.0">
    
    
    
<!--
Kontroll og undersøkelser

2022-02-14 Tone Merete Bruvik (TMB), Universietetsbiblioteket i Bergen.

Undersøker Lind med tanke på hva som finnes av stedsnavn, og annre interessante ting.

-->

    <xsl:output indent="yes" 
        encoding="UTF-8" 
        method="xml"
        doctype-public="https://tei-c.org/release/xml/tei/custom/schema/relaxng/tei_basic.rng"/>
    
    <xsl:mode on-no-match="shallow-copy"/>
    <xsl:strip-space elements="*"/>
   
    
   <xsl:param name="filtype" />
    
   <xsl:variable name="denne_filen" select=" base-uri(//tei:text)"/>
   
   <xsl:template match="tei:front|tei:back">
   </xsl:template>
    
    <xsl:template match="tei:text">
        
        <xsl:element name="text">
            <xsl:element name="front">
                <xsl:element name="div">
                    <xsl:element name="head">Informasjon om hele teksten</xsl:element>
                    
                    <xsl:element name="list">
                    <!-- Antall forekomster av ulike element -->
                    <xsl:for-each-group select="//*" group-by="node-name()" >
                        <xsl:variable name="element_navnet"><xsl:value-of select="local-name()"/></xsl:variable> 
                            <xsl:element name="item">
                                <xsl:value-of select="$element_navnet"/>
                                <xsl:text>: </xsl:text>
                                <xsl:value-of select="count(current-group())"/>
                            </xsl:element>
                        
                        <!-- Antall forkomster av ulike attributter -->
                        <xsl:for-each-group select="current-group()" group-by="  node-name(attribute::*[3])">
                            
                            <xsl:variable name="attributt_navnet"><xsl:value-of select="current-grouping-key()"/></xsl:variable> 
                            <xsl:element name="item">
                                <xsl:value-of select="$element_navnet"/>      
                                <xsl:text>: </xsl:text>  
                                <xsl:value-of select="$attributt_navnet"/>     
                                <xsl:text>: </xsl:text>              
                                <xsl:value-of select="count(current-group())"/>
                            </xsl:element>
                            
                            <xsl:choose>
                                <xsl:when test="$attributt_navnet eq 'subtype'">
                                    <xsl:for-each-group select="current-group()" group-by="  attribute::subtype[1]">
                                        <xsl:element name="item">
                                            <xsl:value-of select="concat($element_navnet,'-', $attributt_navnet)"/>        
                                            <xsl:text>: </xsl:text>
                                            <xsl:value-of select=" current-grouping-key()"/>     
                                            <xsl:text>: </xsl:text>              
                                            <xsl:value-of select="count(current-group())"/>
                                        </xsl:element>
                                    </xsl:for-each-group>
                                </xsl:when>
                                <xsl:otherwise></xsl:otherwise>
                            </xsl:choose>
                        </xsl:for-each-group>
                        
                        <xsl:for-each-group select="current-group()" group-by="  node-name(attribute::*[2])">
                            <xsl:element name="item">
                                <xsl:value-of select="$element_navnet"/>        
                                <xsl:text>: </xsl:text>
                                <xsl:value-of select=" current-grouping-key()"/>  
                                <xsl:text>: </xsl:text>                 
                                <xsl:value-of select="count(current-group())"/>
                            </xsl:element>
                        </xsl:for-each-group>
                        <xsl:for-each-group select="current-group()" group-by="  node-name(attribute::*[3])">
                            <xsl:element name="item">
                                <xsl:value-of select="$element_navnet"/>    
                                <xsl:text>: </xsl:text>    
                                <xsl:value-of select=" current-grouping-key()"/>    
                                <xsl:text>: </xsl:text>               
                                <xsl:value-of select="count(current-group())"/>
                            </xsl:element>
                        </xsl:for-each-group>
                        
                    </xsl:for-each-group>
                </xsl:element>
              </xsl:element>
            </xsl:element>
            <xsl:apply-templates/>
            
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tei:body">
        <!-- Så er det å se på alle ulike forekomster -->
        
        <xsl:element name="body">
            <xsl:element name="div">
                <xsl:element name="head">Stednavn fordi det er typisk stedsnavnpreposisjon før eller det står at det er ortnamn eller lignende foran.</xsl:element>
                <xsl:element name="list">
                    
                    <xsl:variable name="filverdi"><xsl:value-of select="$filtype"/></xsl:variable>
                    
                    <xsl:message>filtype = <xsl:value-of select="$filtype"/>, </xsl:message>
                <xsl:for-each-group select="//tei:name[attribute::ana eq 'stedsnavn']" group-by=".">
                    <xsl:sort select="."/>
                    
                    <xsl:element name="item">  
                        <xsl:attribute name="ana">
                            <xsl:value-of select="attribute::subtype"/> 
                        </xsl:attribute>
                        
                        <xsl:choose>
                            <xsl:when test="$filtype eq 'ren_xml'">
                                <xsl:element name="placeName">
                                    <!--<xsl:apply-templates select="@*"/>-->
                                    <xsl:value-of select="."/>
                                </xsl:element> 
                                
                                <xsl:element name="num">
                                    <xsl:attribute name="ana">forkomster</xsl:attribute>
                                    <xsl:value-of select="count(current-group())"/>
                                </xsl:element>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="normalize-space(current-grouping-key())"/><xsl:text>; </xsl:text> 
                                <xsl:value-of select="count(current-group())"/><xsl:text>; </xsl:text> 
                        
                            </xsl:otherwise>
                        </xsl:choose>
                        
                        <xsl:for-each select=" current-group()">
                            <!-- Referanse til entry i Lind -->
                            <xsl:choose>
                                <xsl:when test="$filtype eq 'ren_xml'">
                                    <xsl:element name="bibl">
                                            <xsl:apply-templates select="normalize-space(ancestor::tei:entryFree/attribute::source)"/>
                                        
                                        <xsl:value-of select=" ancestor::tei:entryFree/tei:form/tei:orth"/>
                                    </xsl:element>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="normalize-space(ancestor::tei:entryFree/attribute::source)"/><xsl:text>; </xsl:text>
                                    <xsl:value-of select=" ancestor::tei:entryFree/tei:form/tei:orth"/><xsl:text>; </xsl:text> 
                                </xsl:otherwise>
                            </xsl:choose>
                            
                                        
                            <!-- Referanse til kilden i Lind -->
                            <xsl:for-each select="following-sibl">
           ing::tei:bibl[1]">                     <xsl:choose>
                                    <xsl:when test="$filtype eq 'ren_xml'">
                                            <xsl:copy-of select="."/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select=" normalize-space(following-sibling::tei:bibl[1]/tei:ref)"/><xsl:text> </xsl:text>
                                        <xsl:value-of select="normalize-space(./attribute::unit)"/><xsl:text>_</xsl:text> 
                                                <xsl:value-of select="normalize-space(.)"/><xsl:text>;</xsl:text> 
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:for-each>
                            <!-- Lager kwic opplegg -->
                            
                            <xsl:call-template name="kwic"/>
                        </xsl:for-each>
                    </xsl:element>
                    
                </xsl:for-each-group>
                </xsl:element>
                
            </xsl:element>
            
            <!-- Stedsnavn som del av personnavn -->
            
                
            <xsl:variable name="liste_gårdsnavn_fra_personnavn">
                <xsl:element name="list">
                    <xsl:for-each-group select="//tei:name[attribute::ana eq 'personnavn']" group-by=".">
                        <xsl:sort select="."/>
                        
                        <xsl:choose>
                            <xsl:when test="matches(.,' (paa|pa|på|a|á|af|fra|från|ort|i|í|j|ór) ')">
                                <xsl:element name="item">  
                                    <xsl:analyze-string select="." regex="  (paa|pa|på|a|á|af|fra|från|ort|i|í|j|ór) (.+)">
                                    <xsl:matching-substring>
                                        <xsl:attribute name="ana" select="concat('stedsnavn_i_personnavn-',regex-group(1))"/>
                                        <xsl:variable name="stedsdel" select="regex-group(2)"/>
                                            <xsl:value-of select=" normalize-space($stedsdel)"/><xsl:text>; </xsl:text>    
                                            <xsl:value-of select=" normalize-space(current-grouping-key())"/><xsl:text>; </xsl:text>   
                                        
                                    </xsl:matching-substring>
                                </xsl:analyze-string>
                                    
                                    <!-- Referanse til entry i Lind -->
                                    <xsl:value-of select="normalize-space(ancestor::tei:entryFree/attribute::source)"/><xsl:text>; </xsl:text>
                                    <xsl:value-of select=" ancestor::tei:entryFree/tei:form/tei:orth"/><xsl:text>; </xsl:text> 
                                    <!-- Referanse til kilden i Lind -->
                                    <xsl:value-of select=" normalize-space(following-sibling::tei:bibl[1]/tei:ref)"/><xsl:text> </xsl:text>
                                    <xsl:for-each select="following-sibling::tei:bibl[1]/tei:biblScope">
                                        <xsl:value-of select="normalize-space(./attribute::unit)"/><xsl:text>_</xsl:text> 
                                        <xsl:value-of select="normalize-space(.)"/><xsl:text>;</xsl:text> 
                                    </xsl:for-each>
                                    
                                    <xsl:call-template name="kwic"/>  
                                </xsl:element>
                            </xsl:when>
                            <xsl:when test="matches(.,'ort(\S+) ')">
                                
                                <xsl:element name="item">  
                                <xsl:analyze-string select="." regex=" ort(\S+) (.+)">
                                    <xsl:matching-substring>
                                        <xsl:attribute name="ana">stedsnavn_i_personnavn_ort</xsl:attribute>
                                        <xsl:variable name="stedsdel" select=" regex-group(2)"/>
                                        <xsl:value-of select=" normalize-space($stedsdel)"/><xsl:text>; </xsl:text>    
                                        <xsl:value-of select=" normalize-space(current-grouping-key())"/><xsl:text>; </xsl:text>   
                                            
                                           
                                    </xsl:matching-substring>
                                </xsl:analyze-string> 
                                    <!-- Referanse til entry i Lind -->
                                    <xsl:value-of select="normalize-space(ancestor::tei:entryFree/attribute::source)"/><xsl:text>; </xsl:text>
                                    <xsl:value-of select=" ancestor::tei:entryFree/tei:form/tei:orth"/><xsl:text>; </xsl:text> 
                                    
                                    <!-- Referanse til kilden i Lind -->
                                    <xsl:value-of select=" normalize-space(following-sibling::tei:bibl[1]/tei:ref)"/><xsl:text> </xsl:text>
                                    <xsl:for-each select="following-sibling::tei:bibl[1]/tei:biblScope">
                                        <xsl:value-of select="normalize-space(./attribute::unit)"/><xsl:text>_</xsl:text> 
                                        <xsl:value-of select="normalize-space(.)"/><xsl:text>;</xsl:text> 
                                    </xsl:for-each>
                                    
                                    <xsl:call-template name="kwic"/>  
                                </xsl:element>
                            </xsl:when>
                            <xsl:when test="matches(.,'gårds(\S+) ')">
                                <xsl:element name="item">  
                                <xsl:analyze-string select="." regex=" gårds(\S+) (.+)">
                                    <xsl:matching-substring>
                                        <xsl:attribute name="ana">stedsnavn_i_personnavn_gård</xsl:attribute>
                                        <xsl:variable name="stedsdel" select=" regex-group(2)"/>
                                            <xsl:value-of select=" normalize-space($stedsdel)"/><xsl:text>; </xsl:text>    
                                            <xsl:value-of select=" normalize-space(current-grouping-key())"/><xsl:text>; </xsl:text>   
                                            
                                        
                                    </xsl:matching-substring>
                                </xsl:analyze-string>
                                    
                                    <!-- Referanse til entry i Lind -->
                                    <xsl:value-of select="normalize-space(ancestor::tei:entryFree/attribute::source)"/><xsl:text>; </xsl:text>
                                    <xsl:value-of select=" ancestor::tei:entryFree/tei:form/tei:orth"/><xsl:text>; </xsl:text> 
                                    <!-- Referanse til kilden i Lind -->
                                    <xsl:value-of select=" normalize-space(following-sibling::tei:bibl[1]/tei:ref)"/><xsl:text> </xsl:text>
                                    <xsl:for-each select="following-sibling::tei:bibl[1]/tei:biblScope">
                                        <xsl:value-of select="normalize-space(./attribute::unit)"/><xsl:text>_</xsl:text> 
                                        <xsl:value-of select="normalize-space(.)"/><xsl:text>;</xsl:text> 
                                    </xsl:for-each>
                                    
                                    <xsl:call-template name="kwic"/>  
                                </xsl:element>
                            </xsl:when> 
                            <!-- Sjekker etter typiske stedsnavn-endelser som 'stad', 'seter', 'lid', 'nes' -->
                            <xsl:when test="matches(.,'(\S+)(heimi|heimum|heimr|rud|rudh|stad|staðir|stader|stadhi|stadhir|stadi|stadir|seter|sæter|sætr|setr|vik),?( í)? ?$')">
                                <xsl:element name="item">  
                                    <xsl:analyze-string select="." regex="(\S+)(heimi|heimum|heimr|rud|rudh|stad|staðir|staðr|stader|stadhi|stadhir|stadi|stadir|seter|sæter|sætr|setr|vik),?$">
                                        <xsl:matching-substring>
                                            
                                            <xsl:attribute name="ana" select="'stedsnavn-endelse-',  regex-group(2)"/>
                                            <xsl:variable name="stedsdel" select="."/>
                                            <xsl:choose>
                                                <xsl:when test="$filtype eq 'ren_xml'">
                                                    <xsl:element name="placeName">
                                                        <xsl:value-of select="normalize-space($stedsdel)"/>
                                                    </xsl:element>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:value-of select="normalize-space($stedsdel)"/><xsl:text>; </xsl:text>    
                                                    <xsl:value-of select=" normalize-space(current-grouping-key())"/><xsl:text>; </xsl:text>   
                                                </xsl:otherwise>
                                            </xsl:choose>
                                            
                                        </xsl:matching-substring>
                                    </xsl:analyze-string>
                                    
                                    <!-- Referanse til entry i Lind -->
                                    <xsl:choose>
                                        <xsl:when test="$filtype eq 'ren_xml'">
                                            <xsl:element name="bibl">
                                                <xsl:attribute name="source">
                                                    <xsl:value-of select="ancestor::tei:entryFree/attribute::source"/></xsl:attribute>
                                                <xsl:value-of select="ancestor::tei:entryFree//tei:orth"/>
                                            </xsl:element>
                                            
                                            <!-- Referanse til kilden oppgitt i Lind -->
                                            <xsl:copy-of select=" following-sibling::tei:bibl[1]"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="normalize-space(ancestor::tei:entryFree/attribute::source)"/><xsl:text>; </xsl:text>
                                            <xsl:value-of select=" ancestor::tei:entryFree/tei:form/tei:orth"/><xsl:text>; </xsl:text> 
                                            <!-- Referanse til kilden i Lind -->
                                            <xsl:value-of select=" normalize-space(following-sibling::tei:bibl[1]/tei:ref)"/><xsl:text> </xsl:text>
                                            <xsl:for-each select="following-sibling::tei:bibl[1]/tei:biblScope">
                                                <xsl:value-of select="normalize-space(./attribute::unit)"/><xsl:text>_</xsl:text> 
                                                <xsl:value-of select="normalize-space(.)"/><xsl:text>;</xsl:text> 
                                            </xsl:for-each>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    
                                    <xsl:call-template name="kwic"/>  
                                </xsl:element>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:element name="item">  
                                    <xsl:attribute name="ana">personnavn</xsl:attribute>
                                    <xsl:choose>
                                        <xsl:when test="$filtype eq 'ren_xml'">
                                            <xsl:copy-of select="ancestor::tei:entryFree/attribute::source"/>
                                            <xsl:element name="persName">
                                                <xsl:copy-of select=" current-grouping-key()"/>
                                            </xsl:element>
                                            <!-- Referanse til entry i Lind -->
                                            <xsl:element name="bibl">
                                                <xsl:apply-templates select="ancestor::tei:entryFree/attribute::source"/>
                                                <xsl:value-of select="ancestor::tei:entryFree//tei:orth"/>
                                            </xsl:element>
                                            
                                            <!-- Referanse til kilden oppgitt i Lind -->
                                            <xsl:copy-of select=" following-sibling::tei:bibl[1]"/>
                                        </xsl:when>
                                             <xsl:otherwise>
                                         
                                         <xsl:value-of select=" normalize-space(current-grouping-key())"/><xsl:text>; </xsl:text>   
                                                 
                                         <!-- Referanse til entry i Lind -->
                                         <xsl:value-of select="normalize-space(ancestor::tei:entryFree/attribute::source)"/><xsl:text>; </xsl:text>
                                         <xsl:value-of select=" ancestor::tei:entryFree/tei:form/tei:orth"/><xsl:text>; </xsl:text> 
                                         <!-- Referanse til kilden i Lind -->
                                         <xsl:value-of select=" normalize-space(following-sibling::tei:bibl[1]/tei:ref)"/><xsl:text> </xsl:text>
                                         <xsl:for-each select="following-sibling::tei:bibl[1]/tei:biblScope">
                                             <xsl:value-of select="normalize-space(./attribute::unit)"/><xsl:text>_</xsl:text> 
                                             <xsl:value-of select="normalize-space(.)"/><xsl:text>;</xsl:text> 
                                         </xsl:for-each>
                                         
                                             </xsl:otherwise>
                                            </xsl:choose>
                                    <xsl:call-template name="kwic"/>  
                                     </xsl:element>
                                 </xsl:otherwise>
                             </xsl:choose>
                        
                         </xsl:for-each-group>
                     </xsl:element>
                </xsl:variable>
            
            <xsl:element name="div">
                <xsl:element name="head">Stedsnavn som del av personnavn</xsl:element>
                
                <xsl:element name="list">
                    <xsl:for-each select="$liste_gårdsnavn_fra_personnavn//tei:item[contains(attribute::ana, 'stedsnavn')]"><!--
                        <xsl:sort select="attribute::ana"/>-->
                        <xsl:sort select="."/>
                        <xsl:element name="item">
                            <xsl:copy-of select="attribute::ana"></xsl:copy-of>
                            <xsl:apply-templates/>
                        </xsl:element>
                    </xsl:for-each>
                </xsl:element>
            
            </xsl:element>
            
            <!-- Så er det å se på alle forekomster av navn som ikke er vurdert å innholde stedsnavn-->
            
                <!--<xsl:element name="div">
                    <xsl:element name="head">Personnavn som ikke er vurdert å innholde stedsnavn</xsl:element>
                    
                    <xsl:element name="list">
                        <xsl:for-each select="$liste_gårdsnavn_fra_personnavn//tei:item[attribute::ana eq 'personnavn']">
                            <xsl:sort select="."/>
                            <xsl:element name="item">
                                <xsl:copy-of select="attribute::ana"></xsl:copy-of>
                                <xsl:apply-templates/>
                            </xsl:element>
                        </xsl:for-each>
                    </xsl:element>-->
                
            </xsl:element>
            
            
            <!--<xsl:element name="div">
                <xsl:element name="head">Alle personnavnene</xsl:element>
                <xsl:element name="list">
                    <xsl:for-each-group select="//tei:name[attribute::ana eq 'personnavn']" group-by=".">
                        <xsl:sort select="."/>
                        <xsl:element name="item">    
                            <xsl:value-of select=" normalize-space(current-grouping-key())"/><xsl:text>; </xsl:text>   
                            
                            <!-\- Referanse til kilden i Lind -\->
                            <xsl:value-of select=" normalize-space(following-sibling::tei:bibl[1]/tei:ref)"/><xsl:text> </xsl:text>
                            <xsl:for-each select="following-sibling::tei:bibl[1]/tei:biblScope">
                                <xsl:value-of select="normalize-space(./attribute::unit)"/><xsl:text>_</xsl:text> 
                                <xsl:value-of select="normalize-space(.)"/><xsl:text>;</xsl:text> 
                            </xsl:for-each>
                        </xsl:element>
                        
                    </xsl:for-each-group>
                </xsl:element>
            </xsl:element>-->
   </xsl:template>
        
    <xsl:template match="attribute::opt|attribute::full|attribute::instant|attribute::default|attribute::status">
        <!-- Ignorer -->
    </xsl:template>


    <xsl:template name="kwic">
        <xsl:element name="s">
            <xsl:attribute name="ana">kwic</xsl:attribute>
            <xsl:variable name="tekst_etter">
                
                <xsl:value-of select=" following::text()[1]"/><xsl:text> </xsl:text>
                <xsl:value-of select=" following::text()[2]"/><xsl:text> </xsl:text>
                <xsl:value-of select=" following::text()[3]"/><xsl:text> </xsl:text>
                <xsl:value-of select=" following::text()[4]"/><xsl:text> </xsl:text>
                <xsl:value-of select=" following::text()[5]"/><xsl:text> </xsl:text>
                <xsl:value-of select=" following::text()[6]"/><xsl:text> </xsl:text>
                <xsl:value-of select=" following::text()[7]"/><xsl:text> </xsl:text>
                <xsl:value-of select=" following::text()[8]"/><xsl:text> </xsl:text>
                <xsl:value-of select=" following::text()[9]"/><xsl:text> </xsl:text>
                <xsl:value-of select=" following::text()[10]"/><xsl:text> </xsl:text>
            </xsl:variable>
            
            <xsl:variable name="tekst_før">
                <xsl:value-of select="preceding::text()[10]"/><xsl:text> </xsl:text>
                <xsl:value-of select="preceding::text()[9]"/><xsl:text> </xsl:text>
                <xsl:value-of select="preceding::text()[8]"/><xsl:text> </xsl:text>
                <xsl:value-of select="preceding::text()[7]"/><xsl:text> </xsl:text>
                <xsl:value-of select="preceding::text()[6]"/><xsl:text> </xsl:text>
                <xsl:value-of select="preceding::text()[5]"/><xsl:text> </xsl:text>
                <xsl:value-of select="preceding::text()[4]"/><xsl:text> </xsl:text>
                <xsl:value-of select="preceding::text()[3]"/><xsl:text> </xsl:text>
                <xsl:value-of select="preceding::text()[2]"/><xsl:text> </xsl:text>
                <xsl:value-of select="preceding::text()[1]"/><xsl:text> </xsl:text>
            </xsl:variable>
            
            <xsl:value-of select="substring( normalize-space($tekst_før), last()-100)"/>
            <xsl:text> </xsl:text>
            <xsl:element name="hi">
                <xsl:value-of select="."/></xsl:element>
            <xsl:text> </xsl:text>
            <xsl:value-of select=" substring(normalize-space($tekst_etter), 0,100)"/>
        </xsl:element>
    </xsl:template>


    
    
  </xsl:stylesheet>
