<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:ubfu="http://uib.no/ub/"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.tei-c.org/ns/1.0"
    version="3.0">
    
    
    
<!--
Kontroll og undersøkelser

2022-02-14 Tone Merete Bruvik (TMB), Universietetsbiblioteket i Bergen.

Undersøker Lind med tanke på hva som finnes av stedsnavn, og annre interessante ting.

-->

    <xsl:output indent="yes" 
        encoding="UTF-8" 
        method="xml"
        doctype-public="https://tei-c.org/release/xml/tei/custom/schema/relaxng/tei_basic.rng"/>
    
    <xsl:mode on-no-match="shallow-copy"/>
    <xsl:strip-space elements="*"/>
   
    
   
    <xsl:variable name="denne_filen" select=" base-uri(//tei:text)"/>
   
   <xsl:template match="tei:front|tei:back">
   </xsl:template>
    
    <xsl:template match="tei:text">
        <xsl:element name="kontroll">
            <xsl:comment>Informasjon om hele teksten</xsl:comment><!--
            <xsl:element name="antall">
                <xsl:attribute name="type">diplom</xsl:attribute>
                <xsl:value-of select="count(//tei:TEI)"/>
            </xsl:element>
               
           <xsl:element name="antall">
               <xsl:attribute name="type">lb</xsl:attribute>
               <xsl:value-of select="count(//tei:lb)"/>
           </xsl:element>
           <xsl:element name="antall">
               <xsl:attribute name="type">w</xsl:attribute>
               <xsl:value-of select="count(//tei:w)"/>
           </xsl:element>
           <xsl:element name="antall">
               <xsl:attribute name="type">ex</xsl:attribute>
               <xsl:value-of select="count(//tei:ex)"/>
           </xsl:element>-->
            
            <!-- Antall forekomster av ulike element -->
            <xsl:for-each-group select="//*" group-by="node-name()" >
                <xsl:variable name="element_navnet"><xsl:value-of select="local-name()"/></xsl:variable> 
                    <xsl:element name="antall">
                        <xsl:attribute name="type"><xsl:value-of select="$element_navnet"/></xsl:attribute>              
                        <xsl:value-of select="count(current-group())"/>
                    </xsl:element>
                
                <!-- Antall forkomster av ulike attribtter -->
                <xsl:for-each-group select="current-group()" group-by="  node-name(attribute::*[1])">
                    
                    <xsl:variable name="attributt_navnet"><xsl:value-of select="current-grouping-key()"/></xsl:variable> 
                    <xsl:element name="antall">
                        <xsl:attribute name="type"><xsl:value-of select="$element_navnet"/></xsl:attribute>        
                        <xsl:attribute name="attributt"><xsl:value-of select=" $attributt_navnet"/></xsl:attribute>                   
                        <xsl:value-of select="count(current-group())"/>
                    </xsl:element>
                    
                    <xsl:choose>
                        <xsl:when test="$attributt_navnet eq 'rend'">
                            <xsl:for-each-group select="current-group()" group-by="  attribute::*[1]">
                                <xsl:element name="antall">
                                    <xsl:attribute name="type"><xsl:value-of select="concat($element_navnet,'-', $attributt_navnet)"/></xsl:attribute>        
                                    <xsl:attribute name="attributt_verdi"><xsl:value-of select=" current-grouping-key()"/></xsl:attribute>                   
                                    <xsl:value-of select="count(current-group())"/>
                                </xsl:element>
                            </xsl:for-each-group>
                        </xsl:when>
                        <xsl:otherwise></xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each-group>
                
                <xsl:for-each-group select="current-group()" group-by="  node-name(attribute::*[2])">
                    <xsl:element name="antall">
                        <xsl:attribute name="type"><xsl:value-of select="$element_navnet"/></xsl:attribute>        
                        <xsl:attribute name="attributt"><xsl:value-of select=" current-grouping-key()"/></xsl:attribute>                   
                        <xsl:value-of select="count(current-group())"/>
                    </xsl:element>
                </xsl:for-each-group>
                <xsl:for-each-group select="current-group()" group-by="  node-name(attribute::*[3])">
                    <xsl:element name="antall">
                        <xsl:attribute name="type"><xsl:value-of select="$element_navnet"/></xsl:attribute>        
                        <xsl:attribute name="attributt"><xsl:value-of select=" current-grouping-key()"/></xsl:attribute>                   
                        <xsl:value-of select="count(current-group())"/>
                    </xsl:element>
                </xsl:for-each-group>
                
            </xsl:for-each-group>
            
            
            
            
            
            <!-- Så må jeg sjekke om det mangler helt sentrale elementer -->
            
            <!--<xsl:for-each select="//tei:TEI">
                <xsl:choose>
                    <xsl:when test="exists( descendant::tei:w) and exists( descendant::tei:ex)"></xsl:when>
                    <xsl:when test="exists( descendant::tei:w)">
                        <xsl:element name="mangel">
                            <xsl:attribute name="type"><xsl:text >tei:ex</xsl:text></xsl:attribute>                   
                            <xsl:value-of select=" normalize-space(descendant::tei:sourceDesc)"/>
                        </xsl:element>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:element name="mangel">
                            <xsl:attribute name="type"><xsl:text >tei:w_og_tei:ex</xsl:text></xsl:attribute>                   
                            <xsl:value-of select=" normalize-space(descendant::tei:sourceDesc)"/>
                        </xsl:element>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>-->
            
        </xsl:element>
        
    </xsl:template>
   <xsl:template match="tei:body">
       <xsl:element name="div">
           <xsl:element name="tei:head">
               <xsl:text>ab med gårdsnavn</xsl:text>
               </xsl:element>
           <!--
            <xsl:element name="antall">
                <xsl:attribute name="type">diplom</xsl:attribute>
                <xsl:value-of select="count(//tei:TEI)"/>
            </xsl:element>
               
           <xsl:element name="antall">
               <xsl:attribute name="type">lb</xsl:attribute>
               <xsl:value-of select="count(//tei:lb)"/>
           </xsl:element>
           <xsl:element name="antall">
               <xsl:attribute name="type">w</xsl:attribute>
               <xsl:value-of select="count(//tei:w)"/>
           </xsl:element>
           <xsl:element name="antall">
               <xsl:attribute name="type">ex</xsl:attribute>
               <xsl:value-of select="count(//tei:ex)"/>
           </xsl:element>-->
           
           <!-- Antall forekomster av ulike element -->
           <xsl:for-each-group select="//*" group-by="node-name()" >
               <xsl:variable name="element_navnet"><xsl:value-of select="local-name()"/></xsl:variable> 
               <xsl:element name="antall">
                   <xsl:attribute name="type"><xsl:value-of select="$element_navnet"/></xsl:attribute>                   
                   <xsl:value-of select="count(current-group())"/>
               </xsl:element>
           </xsl:for-each-group>
           
           <!-- Så må jeg sjekke om det er noen diplomer som mangler helt sentrale elementer -->
           
           <xsl:for-each select="//tei:ab">
               <xsl:choose>
                   <xsl:when test=" contains(., 'gårdsn')">
                       <xsl:copy-of select="."/>
                   </xsl:when>
                   <xsl:otherwise>
                   </xsl:otherwise>
               </xsl:choose>
           </xsl:for-each>
           
       </xsl:element>
       
   </xsl:template>
    
    <!--
    <xsl:template match="*">
        <xsl:apply-templates/>
    </xsl:template>-->
<!--

    <xsl:template match="text()"></xsl:template>-->
    
  </xsl:stylesheet>
