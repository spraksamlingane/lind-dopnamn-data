<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:ubfu="http://uib.no/ub/"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.tei-c.org/ns/1.0"
    version="3.0">
    
    
    
<!--
Kontroll og undersøkelser

2022-02-14 Tone Merete Bruvik (TMB), Universietetsbiblioteket i Bergen.

Undersøker Lind med tanke på hva som finnes av stedsnavn, og annre interessante ting.

-->

    <xsl:output indent="yes" 
        encoding="UTF-8" 
        method="xml"
       />
    
    <xsl:mode on-no-match="shallow-copy"/>
    <xsl:strip-space elements="*"/>
   
    
   
    <xsl:variable name="denne_filen" select=" base-uri(//tei:text)"/>
    
    <xsl:template match="comment()">
        <!-- Fjern disse -->
    </xsl:template>
    
    <xsl:template match="tei:lb">
        <xsl:element name="lb">
            <xsl:apply-templates select="attribute::*"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tei:cb">
        <xsl:element name="cb">
            <xsl:apply-templates select="attribute::*"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tei:pb">
        <xsl:element name="cb">
            <xsl:apply-templates select="attribute::*"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    
    <xsl:template match="tei:div[attribute::type eq 'salute']">
      <!-- Ignor -->
    </xsl:template>
    
    
    <xsl:template match="tei:abbr">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="tei:desc">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="tei:ab">
        <xsl:element name="entryFree">
            <xsl:apply-templates select="attribute::source"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tei:ab" mode="ikke_bruk">
        <xsl:element name="entryFree">
            <xsl:apply-templates select="attribute::*"/>
            <xsl:apply-templates select="tei:title"/>
            <xsl:apply-templates select="tei:abbr[1]/tei:hi[attribute::rend eq 'bold']"/>
            <xsl:apply-templates select="tei:abbr[2]/tei:hi[attribute::rend eq 'bold']"/>
            
            <xsl:variable name="denne_artikkel" select="."/>
            <xsl:for-each select="$denne_artikkel/tei:hi[attribute::rend eq 'bold']">
                
                <xsl:variable name="pre_tekst" select="preceding::text()[1]"/>
                <xsl:element name="desc">
                    <xsl:value-of select="$pre_tekst"/>
                </xsl:element> 
                
                <xsl:choose>
                    <xsl:when test=". eq 'Fing.'">
                        <xsl:element name="usg">
                            <xsl:attribute name="type">dom</xsl:attribute>
                            <xsl:text>Fingert</xsl:text>
                        </xsl:element>
                    </xsl:when>
                    <xsl:otherwise>
                        
                <!-- Dette er en forekomst i en kilde-->
                <xsl:element name="cit">
                    <xsl:attribute name="type">example</xsl:attribute>
                    <xsl:attribute name="ana">
                        <xsl:choose>
                            <xsl:when test="contains($pre_tekst, 'gård')">gårdsnavn</xsl:when>
                            <xsl:when test="contains($pre_tekst, 'ort')">stedsnavn</xsl:when>
                            <xsl:otherwise>
                                <xsl:text>personnavn</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    
                    <xsl:element name="quote"><xsl:value-of select="."/></xsl:element>
                    <xsl:element name="bibl">
                        <xsl:variable name="ref_verk">
                            <xsl:value-of select=" following-sibling::tei:abbr[1]/tei:hi[1]"/>
                        </xsl:variable>
                        
                        <xsl:variable name="verk_navn">
                            <xsl:value-of select="//tei:div[attribute::type eq 'abbr'][1]/tei:p[tei:abbr[1]/tei:hi[1] eq $ref_verk]/tei:desc"/>
                        </xsl:variable>
                        
                        
                        <xsl:element name="abbr">
                            <xsl:value-of select="$ref_verk"/>
                        </xsl:element>
                        
                        <xsl:element name="title">
                            <xsl:value-of select="$verk_navn"/>
                        </xsl:element>
                        <xsl:variable name = "del_side" select="following-sibling::text()[1]"/>
                        
                        <xsl:choose>
                            <xsl:when test=" matches($del_side,'([IVX]+)\s(\d+)')">
                                <xsl:analyze-string select="$del_side" regex="([IVX]+)\s(\d+)">
                                    <xsl:matching-substring>
                                        <xsl:element name="biblScope">
                                            <xsl:attribute name="unit">bind</xsl:attribute>
                                            <xsl:value-of select=" regex-group(1)"/>
                                        </xsl:element>
                                        <xsl:element name="biblScope">
                                            <xsl:attribute name="unit">side</xsl:attribute>
                                            <xsl:value-of select=" regex-group(2)"/>
                                        </xsl:element>
                                    </xsl:matching-substring>
                               </xsl:analyze-string>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:element name="biblScope">
                                    <xsl:attribute name="unit">side</xsl:attribute>
                                    <xsl:value-of select=" following-sibling::text()[1]"/>
                                </xsl:element>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:element name="biblScope">
                            <xsl:attribute name="unit">linje</xsl:attribute>
                            <xsl:value-of select="following-sibling::tei:hi[attribute::rend='sup'][1]"/>
                        </xsl:element>
                    </xsl:element>
                </xsl:element>
                    </xsl:otherwise>
                </xsl:choose>
                
                <xsl:variable name="post_tekst" select="following::text()[1]"/>
                <xsl:element name="desc">
                    <xsl:value-of select="$post_tekst"/>
                </xsl:element>
                
            </xsl:for-each>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tei:ab" mode="ikke_bruk">
        <xsl:element name="entryFree">
            <xsl:attribute name="source"><xsl:value-of select="concat('Lind_p',' preceding::tei:pb[1]','_c_','preceding::tei:cb[1]')"/></xsl:attribute>
            <xsl:apply-templates select="tei:title"/>
            <xsl:apply-templates select="tei:abbr[1]/tei:hi[attribute::rend eq 'bold']"/>
            <xsl:apply-templates select="tei:abbr[2]/tei:hi[attribute::rend eq 'bold']"/>
            
            <xsl:variable name="denne_artikkel" select="."/>
            <xsl:for-each select="$denne_artikkel/tei:hi[attribute::rend eq 'bold']">
                
                <xsl:choose>
                    <xsl:when test=". eq 'Fing.'">
                        <xsl:element name="usg">
                            <xsl:attribute name="type">dom</xsl:attribute>
                            <xsl:text>Fingert</xsl:text>
                        </xsl:element>
                        
                        <!-- Ta med tekst frem til en eventuelt forekomst -->
                        
                        <xsl:variable name="neste_forekomst" select="$denne_artikkel/following::tei:hi[@rend eq 'bold'][1]"/>
                        
                        <xsl:variable name="post_tekst">
                            <xsl:for-each select="$denne_artikkel/following::text()|$denne_artikkel/following::*">
                                <xsl:if test=". lt $neste_forekomst">
                                    <xsl:value-of select="."/>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:variable>
                        
                        <xsl:element name="desc">
                            <xsl:value-of select="$post_tekst"/>
                        </xsl:element>
                    </xsl:when>
                    <xsl:otherwise>
                        
                        <xsl:variable name="pre_tekst" select="preceding::text()[1]"/>
                        <!-- Dette er en forekomst i en kilde-->
                        <xsl:element name="cit">
                            <xsl:attribute name="type">example</xsl:attribute>
                            <xsl:attribute name="ana">
                                <xsl:choose>
                                    <xsl:when test="contains($pre_tekst, 'gård')">gårdsnavn</xsl:when>
                                    <xsl:when test="contains($pre_tekst, 'ort')">stedsnavn</xsl:when>
                                    <xsl:otherwise>
                                        <xsl:text>personnavn</xsl:text>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:attribute>
                            
                            <xsl:element name="quote"><xsl:value-of select="."/></xsl:element>
                            <xsl:element name="bibl">
                                <xsl:variable name="ref_verk">
                                    <xsl:value-of select=" following-sibling::tei:abbr[1]/tei:hi[1]"/>
                                </xsl:variable>
                                
                                <xsl:variable name="verk_navn">
                                    <xsl:value-of select="//tei:div[attribute::type eq 'abbr'][1]/tei:p[tei:abbr[1]/tei:hi[1] eq $ref_verk]/tei:desc"/>
                                </xsl:variable>
                                
                                
                                <xsl:element name="abbr">
                                    <xsl:value-of select="$ref_verk"/>
                                </xsl:element>
                                
                                <xsl:element name="title">
                                    <xsl:value-of select="$verk_navn"/>
                                </xsl:element>
                                <xsl:variable name = "del_side" select="following-sibling::text()[1]"/>
                                
                                <xsl:choose>
                                    <xsl:when test=" matches($del_side,'([IVX]+)\s(\d+)')">
                                        <xsl:analyze-string select="$del_side" regex="([IVX]+)\s(\d+)">
                                            <xsl:matching-substring>
                                                <xsl:element name="biblScope">
                                                    <xsl:attribute name="unit">bind</xsl:attribute>
                                                    <xsl:value-of select=" regex-group(1)"/>
                                                </xsl:element>
                                                <xsl:element name="biblScope">
                                                    <xsl:attribute name="unit">side</xsl:attribute>
                                                    <xsl:value-of select=" regex-group(2)"/>
                                                </xsl:element>
                                            </xsl:matching-substring>
                                        </xsl:analyze-string>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:element name="biblScope">
                                            <xsl:attribute name="unit">side</xsl:attribute>
                                            <xsl:value-of select=" following-sibling::text()[1]"/>
                                        </xsl:element>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <xsl:element name="biblScope">
                                    <xsl:attribute name="unit">linje</xsl:attribute>
                                    <xsl:value-of select="following-sibling::tei:hi[attribute::rend='sup'][1]"/>
                                </xsl:element>
                            </xsl:element>
                        </xsl:element>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tei:ab/tei:title">
        <xsl:element name="form">
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tei:ab/tei:title/tei:hi">
        <xsl:choose>
            <xsl:when test="contains(.,',')">
                <xsl:element name="orth">
                    <xsl:value-of select=" substring-before(.,',')"/>
                </xsl:element>
                <xsl:element name="iType">
                    <xsl:value-of select=" substring-after(.,',')"/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="orth">
                    <xsl:value-of select="."/>
                </xsl:element>
                
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="tei:ab/tei:abbr/tei:hi[attribute::rend eq 'bold']">
        <xsl:choose>
            <xsl:when test=". eq 'M.'">
                <xsl:element name="gram">
                    <xsl:attribute name="type">gen</xsl:attribute>
                    <xsl:text>masculine</xsl:text>
                </xsl:element>
            </xsl:when>
            <xsl:when test=". eq 'Kv.'">
                <xsl:element name="gram">
                    <xsl:attribute name="type">gen</xsl:attribute>
                    <xsl:text>feminine</xsl:text>
                </xsl:element>
            </xsl:when>
            <xsl:when test=". eq 'N.'">
                <xsl:element name="usg">
                    <xsl:attribute name="type">geo</xsl:attribute>
                    <xsl:text>Norge</xsl:text>
                </xsl:element>
            </xsl:when>
            <xsl:when test=". eq 'Isl.'">
                <xsl:element name="usg">
                    <xsl:attribute name="type">geo</xsl:attribute>
                    <xsl:text>Island</xsl:text>
                </xsl:element>
            </xsl:when>
            <xsl:when test=". eq 'Fing.'">
                <xsl:element name="usg">
                    <xsl:attribute name="type">dom</xsl:attribute>
                    <xsl:text>Fingert</xsl:text>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="usg">
                    <xsl:attribute name="type">ukjent</xsl:attribute>
                    <xsl:value-of select="."/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="tei:ab/tei:hi[attribute::rend eq 'bold']">
        <xsl:choose>
            <xsl:when test=". eq 'Fing.'">
                <xsl:element name="usg">
                    <xsl:attribute name="type">dom</xsl:attribute>
                    <xsl:text>Fingert</xsl:text>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="pre_tekst">
                    <xsl:value-of select="preceding::text()[1]"/>
                </xsl:variable> 
                
                <xsl:variable name="pre_tekst_array" select="tokenize( $pre_tekst,' ')"/>
                
                <xsl:variable name="siste_ord_før">
                    <xsl:choose>
                        <xsl:when test="$pre_tekst_array[last()] eq ''">
                            <xsl:value-of select="$pre_tekst_array[last() - 1]"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$pre_tekst_array[last()]"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                
                <!-- Dette er en forekomst i en kilde-->
                <xsl:element name="name">
                    <xsl:attribute name="type">example</xsl:attribute>
                    
                    
                    <xsl:variable name="navnetype">
                        <xsl:choose>
                            <xsl:when test="$siste_ord_før eq 'aa'">
                                <xsl:value-of select="concat('stedsnavn_',$siste_ord_før)"/></xsl:when>
                            <xsl:when test="$siste_ord_før eq 'i'">
                                <xsl:value-of select="concat('stedsnavn_',$siste_ord_før)"/></xsl:when>
                            <xsl:when test="$siste_ord_før eq 'j'">
                                <xsl:value-of select="concat('stedsnavn_',$siste_ord_før)"/></xsl:when>
                            <xsl:when test="$siste_ord_før eq 'a'">
                                <xsl:value-of select="concat('stedsnavn_',$siste_ord_før)"/></xsl:when>
                            <xsl:when test="$siste_ord_før eq 'á'">
                                <xsl:value-of select="concat('stedsnavn_',$siste_ord_før)"/></xsl:when>
                            <xsl:when test="$siste_ord_før eq 'fra'">
                                <xsl:value-of select="concat('stedsnavn_',$siste_ord_før)"/></xsl:when>
                            <xsl:when test="$siste_ord_før eq 'från'">
                                <xsl:value-of select="concat('stedsnavn_',$siste_ord_før)"/></xsl:when>
                            <xsl:when test="$siste_ord_før eq 'ór'">
                                <xsl:value-of select="concat('stedsnavn_',$siste_ord_før)"/></xsl:when>
                            <xsl:when test="$siste_ord_før eq 'på'">
                                <xsl:value-of select="concat('stedsnavn_',$siste_ord_før)"/></xsl:when> 
                            <xsl:when test="$siste_ord_før eq 'pa'">
                                <xsl:value-of select="concat('stedsnavn_',$siste_ord_før)"/></xsl:when> 
                            <xsl:when test="$siste_ord_før eq 'försvunnet'">
                                <xsl:value-of select="concat('stedsnavn_',$siste_ord_før)"/></xsl:when>
                            <xsl:when test="$siste_ord_før eq 'nu'">
                                <xsl:value-of select="concat('stedsnavn_',$siste_ord_før)"/></xsl:when>
                            <xsl:when test="$siste_ord_før eq 'gård'">
                                <xsl:value-of select="concat('gårdsnavn_',$siste_ord_før)"/></xsl:when>
                            <xsl:when test="contains($siste_ord_før, 'fylkesnamn')">
                                <xsl:value-of select="concat('stedsnavn_','fylkesnamn')"/></xsl:when>
                            <xsl:when test="contains($siste_ord_før, 'gård')">
                                <xsl:value-of select="concat('gårdsnavn_','gård')"/></xsl:when>
                            <xsl:when test="contains($siste_ord_før, 'förkort')">
                                <xsl:text>personnavn</xsl:text> <!-- For å ikke blande inn 'ort' -->
                            </xsl:when>
                            <xsl:when test="contains($siste_ord_før, 'ort')">
                                <xsl:value-of select="concat('stedsnavn_','ort')"/></xsl:when>
                            <!-- Sjekker om selve navnet innholder en forstavelse som indikerer stedsnavn -->
                            <xsl:when test="starts-with(.,'a ')">stedsnavn_start_a</xsl:when>
                            <xsl:when test="starts-with(.,'aa ')">stedsnavn_start_aa</xsl:when>
                            <xsl:when test="starts-with(.,'á ')">stedsnavn_start_á</xsl:when>
                            <xsl:when test="starts-with(.,'j ')">stedsnavn_start_j</xsl:when>
                            <xsl:when test="starts-with(.,'i ')">stedsnavn_start_i</xsl:when>
                            <xsl:otherwise>
                                <xsl:text>personnavn</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    
                    <xsl:attribute name="ana">
                        <xsl:choose>
                            <xsl:when test="$navnetype eq 'personnavn'"><xsl:text>personnavn</xsl:text></xsl:when>
                            <xsl:otherwise>
                                <xsl:text>stedsnavn</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:attribute>
                    <xsl:attribute name="subtype">
                        <xsl:value-of select="$navnetype"/>
                    </xsl:attribute>
                    
                    <xsl:value-of select="."/>
                </xsl:element>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="tei:ab/tei:abbr/tei:hi[attribute::rend eq 'italic']">
        
        <xsl:variable name="ref_verk">
            <xsl:value-of select="."/>
            <xsl:if test=" following-sibling::tei:hi[attribute::rend eq 'sup']">
                <xsl:value-of select="following-sibling::tei:hi[attribute::rend eq 'sup']"/>
            </xsl:if>
        </xsl:variable>
        
        <xsl:variable name="utgave"  select="//tei:div[@type='abbr']/tei:bibl[tei:idno eq $ref_verk]"/>
        
        <xsl:variable name="verk_navn">
            <xsl:value-of select="//tei:div[@type='abbr']/tei:bibl[tei:idno eq $ref_verk]/tei:title"/>
        </xsl:variable>
        
        <xsl:choose>
            <xsl:when test="$verk_navn eq ''">
                <xsl:copy-of select="."/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="bibl">
                    
                    <xsl:element name="ref">
                        <xsl:attribute name="target"><xsl:value-of select="$utgave/attribute::xml:id"/></xsl:attribute>
                        <xsl:value-of select="$ref_verk"/>
                    </xsl:element>
                    
                   <!-- <xsl:element name="title">
                        <xsl:value-of select="$verk_navn"/>
                    </xsl:element>
                    -->
                    <xsl:variable name="del_side">
                        <xsl:choose>
                            <xsl:when test=" following-sibling::tei:hi[attribute::rend eq 'sup']">
                                <xsl:value-of select="following::text()[2]"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="following::text()[1]"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    
                    <xsl:choose>
                        <xsl:when test=" matches($del_side,'^\s?([IVX]+)\s(\d+)\s?$')">
                            <xsl:analyze-string select="$del_side" regex="([IVX]+)\s(\d+)">
                                <xsl:matching-substring>
                                    <xsl:element name="biblScope">
                                        <xsl:attribute name="unit">volume</xsl:attribute>
                                        <xsl:value-of select=" regex-group(1)"/>
                                    </xsl:element>
                                    <xsl:element name="biblScope">
                                        <xsl:attribute name="unit">page</xsl:attribute>
                                        <xsl:value-of select=" regex-group(2)"/>
                                    </xsl:element>
                                </xsl:matching-substring>
                            </xsl:analyze-string>
                        </xsl:when>
                        <xsl:when test=" matches($del_side,'^\s?([IVX]+)\s(\d+)\s?$')">
                            <xsl:analyze-string select="$del_side" regex="([IVX]+)\s(\d+)">
                                <xsl:matching-substring>
                                    <xsl:element name="biblScope">
                                        <xsl:attribute name="unit">volume</xsl:attribute>
                                        <xsl:value-of select=" regex-group(1)"/>
                                    </xsl:element>
                                    <xsl:element name="biblScope">
                                        <xsl:attribute name="unit">page</xsl:attribute>
                                        <xsl:value-of select=" regex-group(2)"/>
                                    </xsl:element>
                                </xsl:matching-substring>
                            </xsl:analyze-string>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:element name="biblScope">
                                <xsl:attribute name="unit">page</xsl:attribute>
                                <xsl:value-of select="$del_side"/>
                            </xsl:element>
                        </xsl:otherwise>
                    </xsl:choose>
                    
                    <xsl:variable name="eventuelt_linjenr">
                        <xsl:choose>
                            <xsl:when test=" following-sibling::tei:hi[attribute::rend eq 'sup']">
                                <xsl:copy-of select="following::*[2]"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:copy-of select="following::*[1]"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    
                    <xsl:variable name="linjenr">
                        <xsl:value-of select="$eventuelt_linjenr"/>
                    </xsl:variable>
                    
                    <xsl:if test="$eventuelt_linjenr/tei:hi/@rend eq 'sup'">
                        <xsl:if test="matches($linjenr,'\d+')">
                            <xsl:element name="biblScope">
                                <xsl:attribute name="unit">line</xsl:attribute>
                                <xsl:value-of  select="$eventuelt_linjenr"/>
                            </xsl:element>
                        </xsl:if>
                     </xsl:if>
                </xsl:element>
                
                
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="tei:ab/tei:hi[@rend eq 'bold']/tei:hi[attribute::rend eq 'italic']">
        
       
        <xsl:element name="bibl">
            
            <xsl:element name="author"><xsl:value-of select="."/></xsl:element>
            
            <xsl:variable name = "referanse" select="following::text()[1]"/>
            
            <xsl:choose> 
                <xsl:when test=" matches($referanse,'^(\w+)\.\s([IVX]+),\s(\d+)\ss\. (\d+)$')">
                    <xsl:analyze-string select="$referanse" regex="'^(\w+)\.\s([IVX]+),\s(\d+)\ss\. (\d+)$'">
                        <xsl:matching-substring>
                            <xsl:element name="biblTitle">
                                <xsl:value-of select=" regex-group(1)"/>
                            </xsl:element>
                        <xsl:element name="biblScope">
                            <xsl:attribute name="unit">volume</xsl:attribute>
                            <xsl:value-of select=" regex-group(2)"/>
                        </xsl:element>
                        <xsl:element name="biblScope">
                            <xsl:attribute name="unit">page</xsl:attribute>
                            <xsl:value-of select=" regex-group(4)"/>
                        </xsl:element>
                    </xsl:matching-substring>
                </xsl:analyze-string>
            </xsl:when>
                <xsl:otherwise>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:element>
                
    </xsl:template>
    
    <xsl:template match="tei:hi[attribute::rend eq 'sup']">
        <xsl:choose>
            <xsl:when test="matches(.,'\d+')">
                <!-- Ignor hvis det er i linjenr referanser -->
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    
    <xsl:template match="text()">
        <xsl:variable name="tekst" select="."/>
        
        <xsl:choose>
            <xsl:when test="(preceding-sibling::*[1] eq preceding-sibling::tei:abbr[tei:hi[attribute::rend eq 'italic']][1]) and matches(.,'\d+')"></xsl:when>
            <xsl:when test=" matches($tekst,'^ se $')">
                        <xsl:element name="xr">
                            <xsl:value-of select="."/>
                            <xsl:element name="ref">
                                <xsl:apply-templates select="following-sibling::tei:hi"/>
                            </xsl:element>
                        </xsl:element>
            </xsl:when>
            <xsl:when test=" matches($tekst,'^\s?([IVX]+)\s(\d+)\s?(\(\d+\))?$')">
                <xsl:analyze-string select="$tekst" regex="([IVX]+)\s(\d+)\s?(\(\d+\))?">
                    <xsl:matching-substring>
                        <xsl:element name="biblScope">
                            <xsl:attribute name="unit">volume</xsl:attribute>
                            <xsl:value-of select=" regex-group(1)"/>
                        </xsl:element>
                        <xsl:element name="biblScope">
                            <xsl:attribute name="unit">page</xsl:attribute>
                            <xsl:value-of select=" regex-group(2)"/>
                        </xsl:element>
                        <xsl:if test="string( regex-group(3))">
                            <xsl:element name="tei:year">
                                <xsl:value-of select="regex-group(2)"/>
                            </xsl:element>
                        </xsl:if>
                    </xsl:matching-substring>
                </xsl:analyze-string>
            </xsl:when>
            <xsl:when test=" matches($tekst,'^\s?(\d+\s\w+)?$')">
                <xsl:analyze-string select="$tekst" regex="^\s?(\d+\s\w+)?$">
                    <xsl:matching-substring>
                        <xsl:element name="biblScope">
                            <xsl:attribute name="unit">page</xsl:attribute>
                            <xsl:value-of select="$tekst"/>
                        </xsl:element>
                    </xsl:matching-substring>
                </xsl:analyze-string>
            </xsl:when>
            <xsl:when test=" matches($tekst,'^\s?(\d+)\s?(\(\d+\))?$')">
                <xsl:analyze-string select="$tekst" regex="([IVX]+)\s(\d+)(\(\d+\))?">
                    <xsl:matching-substring>
                        <xsl:element name="biblScope">
                            <xsl:attribute name="unit">page</xsl:attribute>
                            <xsl:value-of select="$tekst"/>
                        </xsl:element>
                        <xsl:if test="string( regex-group(2))">
                            <xsl:element name="tei:year">
                                <xsl:value-of select="regex-group(2)"/>
                            </xsl:element>
                        </xsl:if>
                    </xsl:matching-substring>
                </xsl:analyze-string>
            </xsl:when>
            <xsl:otherwise>
                    <xsl:value-of select="$tekst"/>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>
  </xsl:stylesheet>
