<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:ubfu="http://uib.no/ub/"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.tei-c.org/ns/1.0"
    version="3.0">
    
    
    
<!--
Forvask

2022-02-14 Tone Merete Bruvik (TMB), Universietetsbiblioteket i Bergen.

1. 

-->

    <xsl:output indent="yes" 
        encoding="UTF-8" 
        method="xml"
       />
    
    <xsl:mode on-no-match="shallow-copy"/>
    <xsl:strip-space elements="*"/>
   
  
    <xsl:template match="tei:ab/tei:abbr/tei:hi[attribute::rend eq 'italic']">
        
        <xsl:variable name="ref_verk">
            <xsl:value-of select="."/>
        </xsl:variable>
        
        <xsl:variable name="verk_navn">
            <xsl:value-of select="//tei:div[@type='abbr']/tei:bibl[tei:idno eq $ref_verk]/tei:title"/>
        </xsl:variable>
        
        <xsl:choose>
            <xsl:when test="$verk_navn eq ''">
                <xsl:copy-of select="."/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:element name="bibl">
                    
                    <xsl:element name="abbr">
                        <xsl:value-of select="$ref_verk"/>
                    </xsl:element>
                    
                    <xsl:element name="title">
                        <xsl:value-of select="$verk_navn"/>
                    </xsl:element>
                    
                    <xsl:variable name = "del_side" select="following::text()[1]"/>
                    
                    <xsl:choose>
                        <xsl:when test=" matches($del_side,'^\s?([IVX]+)\s(\d+)\s?$')">
                            <xsl:analyze-string select="$del_side" regex="([IVX]+)\s(\d+)">
                                <xsl:matching-substring>
                                    <xsl:element name="biblScope">
                                        <xsl:attribute name="unit">volume</xsl:attribute>
                                        <xsl:value-of select=" regex-group(1)"/>
                                    </xsl:element>
                                    <xsl:element name="biblScope">
                                        <xsl:attribute name="unit">page</xsl:attribute>
                                        <xsl:value-of select=" regex-group(2)"/>
                                    </xsl:element>
                                </xsl:matching-substring>
                            </xsl:analyze-string>
                        </xsl:when>
                        <xsl:when test=" matches($del_side,'^\s?([IVX]+)\s(\d+)\s?$')">
                            <xsl:analyze-string select="$del_side" regex="([IVX]+)\s(\d+)">
                                <xsl:matching-substring>
                                    <xsl:element name="biblScope">
                                        <xsl:attribute name="unit">volume</xsl:attribute>
                                        <xsl:value-of select=" regex-group(1)"/>
                                    </xsl:element>
                                    <xsl:element name="biblScope">
                                        <xsl:attribute name="unit">page</xsl:attribute>
                                        <xsl:value-of select=" regex-group(2)"/>
                                    </xsl:element>
                                </xsl:matching-substring>
                            </xsl:analyze-string>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:element name="biblScope">
                                <xsl:attribute name="unit">page</xsl:attribute>
                                <xsl:value-of select="$del_side"/>
                            </xsl:element>
                        </xsl:otherwise>
                    </xsl:choose>
                    
                    <xsl:variable name="eventuelt_linjenr" select="following::*[1]"/>
                    
                    <xsl:if test="local-name($eventuelt_linjenr) eq 'hi' and $eventuelt_linjenr[@rend eq 'sup'] and matches($eventuelt_linjenr,'\d+')">
                        <xsl:element name="biblScope">
                            <xsl:attribute name="unit">line</xsl:attribute>
                            <xsl:value-of  select="$eventuelt_linjenr"/>
                        </xsl:element>
                    </xsl:if>
                </xsl:element>
                
                
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    
    
  </xsl:stylesheet>
