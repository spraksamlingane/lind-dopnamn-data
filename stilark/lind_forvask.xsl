<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:ubfu="http://uib.no/ub/"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.tei-c.org/ns/1.0"
    version="3.0">
    
    
    
<!--
Forvask

2022-02-14 Tone Merete Bruvik (TMB), Universietetsbiblioteket i Bergen.

1. Fjerner alt av pb, lb og cb, det bare forstyrer strukturen i teksten. Informasjonen herfra slyttes inn som attributter i ab-elementene slik at det blir mulig å se hvor dette befinner seg i boken.


-->

    <xsl:output indent="yes" 
        encoding="UTF-8" 
        method="xml"
       />
    
    <xsl:mode on-no-match="shallow-copy"/>
    <xsl:strip-space elements="*"/>
   
    
   
    <xsl:variable name="denne_filen" select=" base-uri(//tei:text)"/>
   
  
    
    <xsl:template match="tei:back">
    </xsl:template>
    
    
    <xsl:template match="tei:div[attribute::type eq 'abbr']">
        <xsl:element name="div">
            <xsl:attribute name="type">abbr</xsl:attribute>
            <xsl:copy-of select="tei:head"/>
            <xsl:for-each select="tei:p">
                <xsl:choose>
                    <xsl:when test="exists(child::tei:abbr)">
                    <xsl:element name="bibl">
                        <xsl:attribute name="xml:id"><xsl:value-of select="translate(tei:abbr,' –','__')"/></xsl:attribute>
                        <xsl:element name="idno">
                            <xsl:value-of select="tei:abbr"/>
                        </xsl:element>
                        <xsl:element name="title">
                            <xsl:apply-templates select="tei:desc"/>
                        </xsl:element>
                    </xsl:element>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:copy-of select="."></xsl:copy-of>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
            
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tei:ab/tei:hi[attribute::rend eq 'italic']">
        
        <xsl:variable name="ref_verk">
            <xsl:value-of select="."/>
            <xsl:if test=" following-sibling::*[1] eq following-sibling::tei:hi[@rend eq 'sup'][1]">
                <xsl:value-of select="following-sibling::tei:hi[@rend eq 'sup']"/>
            </xsl:if>
        </xsl:variable>
        
        <xsl:variable name="verk_navn">
            <xsl:value-of select="//tei:front//tei:p[tei:abbr/tei:hi[1] eq $ref_verk]/tei:desc"/>
        </xsl:variable>
        
        <xsl:choose>
            <xsl:when test=" string-length($verk_navn) > 2">
                <!-- Sjekker om denne treffer i kilde-listen, i såfall er den en feilkoding her som må rettes. -->
                <xsl:element name="abbr">
                    <xsl:copy-of select="."/>
                </xsl:element>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="tei:ab">
        <xsl:element name="ab">
            <xsl:attribute name="source">
                <xsl:value-of select="concat('Lind_c_',preceding::tei:cb[1]/@n)"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="tei:ab/tei:hi[@rend eq 'b']">
        
        <xsl:choose>
            <xsl:when test=" following-sibling::*[1] "></xsl:when>
        </xsl:choose>
        
    </xsl:template>
    
    <xsl:template match="tei:div[attribute::type eq 'salute']">
        <!-- Ignor -->
    </xsl:template>
    
            
    <xsl:template match="tei:lb">
        <!-- Ignor -->
    </xsl:template>
    
    <xsl:template match="tei:cb">
        <!-- Ignor -->
    </xsl:template>
    
    <xsl:template match="tei:pb">
        <!-- Ignor -->
    </xsl:template>
    
    <xsl:template match="tei:div">
       <xsl:apply-templates/>
    </xsl:template>
    
    
  </xsl:stylesheet>
