# Gjøre bruk av Lind som kilde til person og stedsnavn

Lind er bokutgave (Norsk-isländska dopnamn ock fingerade namn från medeltiden
E.H. Lind
1905-1915) som er blitt digitalisert og som vi skal prøve å utnytte  primært som kilde til stedsnavn.

Oppgaven er initiert av Peder Gammeltoft, Språksamlingene ved UB, UiB.

Det er laget en god avskrift som er korrekturlest (?) og som finnes i xml-versjon. Denne skal beharbeides videre.
